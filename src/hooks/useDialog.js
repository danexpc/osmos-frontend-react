import { useState } from 'react'

const useDialog = () => {
  const [openAdd, setOpenAdd] = useState(false)
  const [openEdit, setOpenEdit] = useState(false)

  const handleOpenAddDialog = () => {
    setOpenAdd(true)
  }

  const handleCloseAddDialog = () => {
    setOpenAdd(false)
  }

  const handleOpenEditDialog = () => {
    setOpenEdit(true)
  }

  const handleCloseEditDialog = () => {
    setOpenEdit(false)
  }

  return {
    openAdd,
    openEdit,
    handleOpenAddDialog,
    handleCloseAddDialog,
    handleOpenEditDialog,
    handleCloseEditDialog,
  }
}

export default useDialog

import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { OrderType } from '../common/enums/app/order-type.enum'
import { switchOrderType } from '../utils/utils'

const useSort = () => {
  const dispatch = useDispatch()

  const [orderType, setOrderType] = useState(OrderType.NONE)
  const [orderBy, setOrderBy] = useState(null)

  const handleRequestSort = (property, action) => {
    const { orderBy: newOrderBy, orderType: newOrderType } = switchOrderType(
      orderBy,
      property,
      orderType
    )
    setOrderType(newOrderType)
    setOrderBy(newOrderBy)
    dispatch(action(newOrderBy, newOrderType))
  }

  return {
    orderBy,
    orderType,
    handleRequestSort,
  }
}

export default useSort

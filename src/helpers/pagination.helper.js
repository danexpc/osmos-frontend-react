export const countPagesAmount = (total, pageSize) => Math.ceil(total / pageSize)

export const calculateOffset = (page, pageSize) => (page - 1) * pageSize

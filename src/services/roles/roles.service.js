import { get } from 'helpers/http.helper'

const Roles = (() => {
  const getRoles = (params) =>
    get('/user-service/roles', {
      params,
    })

  return {
    getRoles,
  }
})()

export default Roles

import { get } from 'helpers/http.helper'

const Branches = (() => {
  const getBranches = (params) =>
    get('/rodms-api/branches', {
      params,
    })

  const getBranch = (id) => get(`/rodms-api/branches/${id}`)

  const getDevices = (params) =>
    get(`/rodms-api/branches/${params.branchId}/devices`, {
      params,
    })

  return {
    getBranches,
    getBranch,
    getDevices,
  }
})()

export default Branches

import { get } from 'helpers/http.helper'

const Users = (() => {
  const getUsers = (params) =>
    get('/user-service/rodms/users', {
      params,
    })

  return {
    getUsers,
  }
})()

export default Users

import { post } from 'helpers/http.helper'

const Auth = (() => {
  const login = (request) => post('/user-service/rodms/login', request)

  const refreshToken = (request) =>
    post('/user-service/rodms/refresh-token', request)

  return {
    login,
    refreshToken,
  }
})()

export default Auth

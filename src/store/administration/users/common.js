const ActionType = {
  GET_USERS: 'administration/get-users',
  GET_USER: 'administration/get-user',
  CREATE_USER: 'administration/post-user',
  EDIT_USER: 'administration/put-user',
}

export default ActionType

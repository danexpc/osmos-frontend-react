import { createReducer, isAnyOf } from '@reduxjs/toolkit'

import { LoadingStatus } from 'common/enums/enums'
import { getUsers } from './actions'

const initialState = {
  users: null,
  validationError: null,
  loading: LoadingStatus.IDLE,
}

/* eslint no-param-reassign: off */
const reducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getUsers.fulfilled, (state, action) => {
      state.users = action.payload
      state.loading = LoadingStatus.SUCCEEDED
    })
    .addMatcher(isAnyOf(getUsers.pending), (state) => {
      state.loading = LoadingStatus.LOADING
    })
    .addMatcher(isAnyOf(getUsers.rejected), (state) => {
      state.loading = LoadingStatus.FAILED
    })
})

export { reducer }

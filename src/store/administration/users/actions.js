import { createAsyncThunk } from '@reduxjs/toolkit'
import ActionType from './common'

const getUsers = createAsyncThunk(
  ActionType.GET_USERS,
  async (params, { extra: { services } }) => {
    const { data } = await services.users.getUsers(params)

    return data
  }
)

export { getUsers }

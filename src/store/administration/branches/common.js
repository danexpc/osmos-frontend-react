const ActionType = {
  GET_BRANCHES: 'administration/get-branch',
  GET_BRANCH: 'administration/get-branch',
  CREATE_BRANCH: 'administration/post-branch',
  EDIT_BRANCH: 'administration/put-branch',
}

export default ActionType

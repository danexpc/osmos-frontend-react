import { createAsyncThunk } from '@reduxjs/toolkit'
import ActionType from './common'

const getBranches = createAsyncThunk(
  ActionType.GET_BRANCHES,
  async (params, { extra: { services } }) => {
    const { data } = await services.branches.getBranches(params)

    return data
  }
)

const getBranch = createAsyncThunk(
  ActionType.GET_BRANCH,
  async (id, { extra: { services } }) => services.branches.getBranch(id)
)

export { getBranches, getBranch }

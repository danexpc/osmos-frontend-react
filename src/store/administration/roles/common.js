const ActionType = {
  GET_ROLES: 'administration/get-roles',
  GET_ROLE: 'administration/get-role',
  CREATE_ROLE: 'administration/post-role',
  EDIT_ROLE: 'administration/put-role',
}

export default ActionType

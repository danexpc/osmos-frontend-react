const ActionType = {
  LOAD_BRANCHES: 'header/load-branches',
  SET_SELECTED_BRANCH: 'header/set-selected-branch',
}

export default ActionType

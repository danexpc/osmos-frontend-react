import { createReducer, isAnyOf } from '@reduxjs/toolkit'

import { LoadingStatus } from 'common/enums/enums'
import { login, logout } from './actions'

const initialState = {
  authUser: null,
  validationError: null,
  loading: LoadingStatus.IDLE,
}

/* eslint no-param-reassign: off */
const reducer = createReducer(initialState, (builder) => {
  builder
    .addCase(login.fulfilled, (state, action) => {
      state.authUser = action.payload.data
      state.validationError = null
      state.loading = LoadingStatus.SUCCEEDED
    })
    .addCase(logout.fulfilled, (state) => {
      state.authUser = null
    })
    .addMatcher(isAnyOf(login.pending), (state) => {
      state.loading = LoadingStatus.LOADING
    })
    .addMatcher(isAnyOf(login.rejected), (state, action) => {
      state.loading = LoadingStatus.FAILED
      state.validationError = action.error
    })
})

export { reducer }

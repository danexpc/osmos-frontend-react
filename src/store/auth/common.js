const ActionType = {
  LOG_IN: 'auth/log-in',
  LOG_OUT: 'auth/log-out',
}

export default ActionType

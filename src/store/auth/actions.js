import { createAsyncThunk } from '@reduxjs/toolkit'
import { StorageKey } from 'common/enums/enums'
import ActionType from './common'

const login = createAsyncThunk(
  ActionType.LOG_IN,
  async (request, { extra: { services } }) => {
    const { data } = await services.auth.login(request)

    services.storage.setItem(StorageKey.ACCESS_TOKEN, data.accessToken)
    services.storage.setItem(StorageKey.REFRESH_TOKEN, data.refreshToken)

    const { data: branch } = await services.branches.getBranch(data.branchId)

    return { data, branch }
  }
)

const logout = createAsyncThunk(
  ActionType.LOG_OUT,
  async (request, { extra: { services } }) => {
    services.storage.removeItem(StorageKey.ACCESS_TOKEN)
    services.storage.removeItem(StorageKey.REFRESH_TOKEN)
  }
)

export { login, logout }

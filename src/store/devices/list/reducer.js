import { createReducer, isAnyOf } from '@reduxjs/toolkit'

import { LoadingStatus } from '../../../common/enums/enums'
import { getDevices } from './actions'

const initialState = {
  devices: null,
  validationError: null,
  loading: LoadingStatus.IDLE,
}

/* eslint no-param-reassign: off */
const reducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getDevices.fulfilled, (state, action) => {
      state.devices = action.payload
      state.loading = LoadingStatus.SUCCEEDED
    })
    .addMatcher(isAnyOf(getDevices.pending), (state) => {
      state.loading = LoadingStatus.LOADING
    })
    .addMatcher(isAnyOf(getDevices.rejected), (state) => {
      state.loading = LoadingStatus.FAILED
    })
})

export { reducer }

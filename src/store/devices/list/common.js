const ActionType = {
  GET_DEVICES: 'devices/get-devices',
  GET_DEVICE: 'devices/get-device',
  CREATE_DEVICE: 'devices/post-device',
  EDIT_DEVICE: 'devices/put-device',
}

export default ActionType

import { createAsyncThunk } from '@reduxjs/toolkit'
import ActionType from './common'

const getDevices = createAsyncThunk(
  ActionType.GET_DEVICES,
  async (params, { extra: { services } }) => {
    const { data } = await services.branches.getDevices(params)

    return data
  }
)

export { getDevices }

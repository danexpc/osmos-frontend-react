import { notification } from '../../../services/services'

const handleError = () => (next) => (action) => {
  if (action.error) {
    const { message } = action.error
    notification.error(message)
  }

  return next(action)
}

export { handleError }

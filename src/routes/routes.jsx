import React, { lazy } from 'react'
import { Navigate } from 'react-router-dom'

import Loadable from 'components/helpers/Loadable'
import Roles from 'pages/administration/Roles'

const Login = Loadable(lazy(() => import('pages/auth/Login')))
const DashboardLayout = Loadable(
  lazy(() => import('components/layout/DashboardLayout'))
)
const MinimalLayout = Loadable(
  lazy(() => import('components/layout/MinimalLayout'))
)
const Dashboard = Loadable(lazy(() => import('pages/dashboard/Dashboard')))
const List = Loadable(lazy(() => import('pages/devices/List')))
const DetailedState = Loadable(
  lazy(() => import('pages/devices/DetailedState'))
)
const Branches = Loadable(lazy(() => import('pages/administration/Branches')))
const Users = Loadable(lazy(() => import('pages/administration/Users')))

const routes = (isLoggedIn) => [
  {
    path: '/app',
    element: isLoggedIn ? <DashboardLayout /> : <Navigate to="/login" />,
    children: [
      { path: '/app/dashboard', element: <Dashboard /> },
      { path: '/app/devices/list', element: <List /> },
      { path: '/app/devices/detailed', element: <DetailedState /> },
      { path: '/app/administration/branches', element: <Branches /> },
      { path: '/app/administration/users', element: <Users /> },
      { path: '/app/administration/roles', element: <Roles /> },
      { path: '/app', element: <Navigate to="/app/dashboard" /> },
    ],
  },
  {
    path: '/',
    element: !isLoggedIn ? <MinimalLayout /> : <Navigate to="/app/dashboard" />,
    children: [
      { path: '/login', element: <Login /> },
      { path: '/', element: <Navigate to="/login" /> },
      { path: '/*', element: <Navigate to="/login" /> },
    ],
  },
]

export default routes

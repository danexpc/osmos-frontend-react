import React from 'react'

import { IconRouter, IconStar, IconCalendar } from '@tabler/icons'
import { FormattedMessage } from 'react-intl'

const icons = {
  IconRouter,
  IconStar,
  IconCalendar,
}

const pages = {
  id: 'pages',
  caption: 'Pages Caption',
  type: 'group',
  children: [
    {
      id: 'devices',
      title: <FormattedMessage id="devices" defaultMessage="Devices" />,
      type: 'collapse',
      icon: icons.IconRouter,

      children: [
        {
          id: 'devices_list',
          title: <FormattedMessage id="list" defaultMessage="List" />,
          type: 'item',
          url: '/app/devices/list',
        },
        {
          id: 'detailed_state',
          title: (
            <FormattedMessage
              id="detailed_state"
              defaultMessage="Detailed state"
            />
          ),
          type: 'item',
          url: '/app/devices/detailed',
        },
      ],
    },
    {
      id: 'administration',
      title: (
        <FormattedMessage id="administration" defaultMessage="Administration" />
      ),
      type: 'collapse',
      icon: icons.IconStar,

      children: [
        {
          id: 'branches',
          title: <FormattedMessage id="branches" defaultMessage="Branches" />,
          type: 'item',
          url: '/app/administration/branches',
        },
        {
          id: 'users',
          title: <FormattedMessage id="users" defaultMessage="Users" />,
          type: 'item',
          url: '/app/administration/users',
        },
        {
          id: 'roles',
          title: <FormattedMessage id="roles" defaultMessage="Roles" />,
          type: 'item',
          url: '/app/administration/roles',
        },
      ],
    },
  ],
}

export default pages

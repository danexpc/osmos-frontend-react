import React from 'react'
import { IconDashboard } from '@tabler/icons'
import { FormattedMessage } from 'react-intl'

const icons = { IconDashboard }

const dashboard = {
  id: 'dashboard',
  title: '',
  type: 'group',
  children: [
    {
      id: 'default',
      title: <FormattedMessage id="dashboard" defaultMessage="Dashboard" />,
      type: 'item',
      url: '/app/dashboard',
      icon: icons.IconDashboard,
      breadcrumbs: false,
    },
  ],
}

export default dashboard

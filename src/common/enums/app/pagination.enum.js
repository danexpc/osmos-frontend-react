const PaginationEnum = {
  DEFAULT_OFFSET: 0,
  DEFAULT_LIMIT: 10,
  DEFAULT_PAGE_SIZE: 10,
  DEFAULT_PAGE: 1,
}

export { PaginationEnum }

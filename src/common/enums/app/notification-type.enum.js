const NotificationType = {
  ERROR: 'error',
  SUCCESS: 'success',
}

export { NotificationType }

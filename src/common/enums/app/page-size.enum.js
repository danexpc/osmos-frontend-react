const PageSize = {
  SIZE_10: 10,
  SIZE_20: 20,
  SIZE_50: 50,
}

export { PageSize }

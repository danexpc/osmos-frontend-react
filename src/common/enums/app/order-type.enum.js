const OrderType = {
  ASC: 'asc',
  DESC: 'desc',
  NONE: null,
}

export { OrderType }

import { OrderType, LoadingStatus } from 'common/enums/enums'

export const isLoading = (loading) => loading === LoadingStatus.LOADING

export const switchOrderType = (currentOrderBy, newOrderBy, orderType) => {
  if (currentOrderBy === newOrderBy) {
    if (orderType === OrderType.NONE) {
      return { orderType: OrderType.ASC, orderBy: currentOrderBy }
    }

    if (orderType === OrderType.ASC) {
      return { orderType: OrderType.DESC, orderBy: currentOrderBy }
    }

    return { orderType: OrderType.NONE, orderBy: null }
  }

  return { orderType: OrderType.ASC, orderBy: newOrderBy }
}

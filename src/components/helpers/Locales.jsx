import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

import { IntlProvider } from 'react-intl'
import config from 'config'

const loadLocaleData = (locale) => {
  switch (locale) {
    case 'uk':
      return import('locales/uk.json')
    default:
      return import('locales/en.json')
  }
}

const Locales = ({ children }) => {
  const customization = useSelector((state) => state.customization)
  const [messages, setMessages] = useState()

  useEffect(() => {
    loadLocaleData(customization.locale).then((dict) => {
      setMessages(dict.default)
    })
  }, [customization.locale])

  return (
    <>
      {messages && (
        <IntlProvider
          locale={customization.locale}
          defaultLocale={config.defaultLocale}
          messages={messages}
        >
          {children}
        </IntlProvider>
      )}
    </>
  )
}

Locales.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Locales

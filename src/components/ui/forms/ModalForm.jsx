import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Slide,
} from '@mui/material'

import { gridSpacing } from 'themes/common/constants'
import AnimateButton from 'components/ui/extended/AnimateButton'
import { useSelector } from 'react-redux'

const Transition = forwardRef((props, ref) => (
  <Slide direction="down" ref={ref} {...props} />
))

const ModalForm = ({
  title,
  open,
  children,
  handleCloseDialog,
  handleSubmit,
}) => {
  const customization = useSelector((state) => state.customization)

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleCloseDialog}
      sx={{
        '&>div:nth-of-type(3)': {
          justifyContent: 'center',
          '&>div': {
            m: 0,
            borderRadius: `${customization}px`,
            maxWidth: 450,
            maxHeight: '100%',
          },
        },
      }}
    >
      <form onSubmit={handleSubmit}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <Grid container spacing={gridSpacing} sx={{ mt: 0.25 }}>
            {children}
          </Grid>
        </DialogContent>
        <DialogActions>
          <AnimateButton>
            <Button variant="contained" type="submit">
              Save
            </Button>
          </AnimateButton>
          <Button variant="text" color="error" onClick={handleCloseDialog}>
            Close
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

/* eslint-disable react/forbid-prop-types */
ModalForm.propTypes = {
  title: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  children: PropTypes.array.isRequired,
  handleCloseDialog: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

export default ModalForm

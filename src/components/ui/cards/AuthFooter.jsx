import React from 'react'
import { Typography, Stack } from '@mui/material'

const AuthFooter = () => (
  <Stack direction="row" justifyContent="flex-end">
    <Typography
      variant="subtitle2"
      component="div"
      href="https://berrydashboard.io"
      target="_blank"
      underline="hover"
    >
      © 2022 Ecosoft ®
    </Typography>
  </Stack>
)

export default AuthFooter

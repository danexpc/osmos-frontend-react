import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import { useTheme } from '@mui/material/styles'
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Typography,
} from '@mui/material'

const headerSX = {
  '& .MuiCardHeader-action': { mr: 0 },
}

const MainCard = forwardRef(
  (
    {
      border = true,
      boxShadow,
      children,
      content = true,
      contentClass = '',
      contentSX = {},
      darkTitle,
      secondary,
      shadow,
      sx = {},
      title,
      ...others
    },
    ref
  ) => {
    const theme = useTheme()

    return (
      <Card
        ref={ref}
        {...others}
        sx={{
          border: border ? '1px solid' : 'none',
          borderColor:
            theme.palette.mode === 'dark'
              ? theme.palette.background.default
              : theme.palette.primary[200] + 75,
          ':hover': {
            /* eslint-disable no-nested-ternary */
            boxShadow: boxShadow
              ? theme.palette.mode === 'dark'
                ? '0 2px 14px 0 rgb(33 150 243 / 10%)'
                : '0 2px 14px 0 rgb(32 40 45 / 8%)'
              : 'inherit',
            /* eslint-enable no-nested-ternary */
          },
          ...sx,
        }}
      >
        {!darkTitle && title && (
          <CardHeader sx={headerSX} title={title} action={secondary} />
        )}
        {darkTitle && title && (
          <CardHeader
            sx={headerSX}
            title={<Typography variant="h3">{title}</Typography>}
            action={secondary}
          />
        )}

        {title && <Divider />}

        {content && (
          <CardContent sx={contentSX} className={contentClass}>
            {children}
          </CardContent>
        )}
        {!content && children}
      </Card>
    )
  }
)

/* eslint-disable react/forbid-prop-types */
MainCard.propTypes = {
  border: PropTypes.bool,
  boxShadow: PropTypes.bool,
  children: PropTypes.node.isRequired,
  content: PropTypes.bool,
  contentClass: PropTypes.string,
  contentSX: PropTypes.object,
  darkTitle: PropTypes.bool,
  secondary: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
    PropTypes.object,
  ]),
  shadow: PropTypes.string,
  sx: PropTypes.object,
  title: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
    PropTypes.object,
  ]),
}

MainCard.defaultProps = {
  border: true,
  boxShadow: false,
  content: true,
  contentClass: '',
  contentSX: {},
  darkTitle: false,
  secondary: '',
  shadow: '',
  sx: {},
  title: '',
}

export default MainCard

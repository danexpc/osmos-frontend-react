import React from 'react'
import { styled, useTheme } from '@mui/material/styles'
import { Card, CardContent, Grid, Typography } from '@mui/material'

const CardStyle = styled(Card)(({ theme }) => ({
  background:
    theme.palette.mode === 'dark'
      ? theme.palette.dark[800]
      : theme.palette.warning.light,
  marginTop: '16px',
  marginBottom: '16px',
  overflow: 'hidden',
  position: 'relative',
  '&:after': {
    content: '""',
    position: 'absolute',
    width: '200px',
    height: '200px',
    border: '19px solid ',
    borderColor:
      theme.palette.mode === 'dark'
        ? theme.palette.warning.main
        : theme.palette.warning.main,
    borderRadius: '50%',
    top: '15px',
    right: '-150px',
  },
  '&:before': {
    content: '""',
    position: 'absolute',
    width: '200px',
    height: '200px',
    border: '3px solid ',
    borderColor:
      theme.palette.mode === 'dark'
        ? theme.palette.warning.main
        : theme.palette.warning.main,
    borderRadius: '50%',
    top: '145px',
    right: '-70px',
  },
}))

const WaterQuoteCard = () => {
  const theme = useTheme()

  return (
    <CardStyle>
      <CardContent>
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography
              variant="subtitle2"
              color={
                theme.palette.mode === 'dark' ? 'textSecondary' : 'grey.900'
              }
              sx={{ opacity: 0.8 }}
            >
              “If there is magic on this planet, it is contained in water.”{' '}
              <br />— Loren Eiseley
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </CardStyle>
  )
}

export default WaterQuoteCard

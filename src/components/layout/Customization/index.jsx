import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTheme } from '@mui/material/styles'
import {
  Avatar,
  ButtonBase,
  Drawer,
  Fab,
  FormControl,
  FormControlLabel,
  Grid,
  IconButton,
  Radio,
  RadioGroup,
  Slider,
  Tooltip,
  Typography,
} from '@mui/material'
import { IconChecks, IconSettings } from '@tabler/icons'
import PropTypes from 'prop-types'
import PerfectScrollbar from 'react-perfect-scrollbar'

import { FormattedMessage } from 'react-intl'

import { customizationActionCreator } from 'store/actions'
import SubCard from 'components/ui/cards/SubCard'
import AnimateButton from 'components/ui/extended/AnimateButton'

import colors from 'assets/scss/_themes-vars.module.scss'
import theme6 from 'assets/scss/_theme6.module.scss'
import theme2 from 'assets/scss/_theme2.module.scss'

const valueText = (value) => `${value}px`

const PresetColor = ({ color, presetColor, setPresetColor }) => (
  <Grid item>
    <ButtonBase
      sx={{ borderRadius: '12px' }}
      onClick={() => setPresetColor(color.id)}
    >
      <Avatar
        variant="rounded"
        color="inherit"
        sx={{
          background: `linear-gradient(135deg, ${color.primary} 50%, ${color.secondary} 50%)`,
          opacity: presetColor === color.id ? 0.6 : 1,
        }}
      >
        {presetColor === color.id && <IconChecks color="#fff" />}
      </Avatar>
    </ButtonBase>
  </Grid>
)

PresetColor.propTypes = {
  color: PropTypes.shape({
    id: PropTypes.string,
    primary: PropTypes.string,
    secondary: PropTypes.string,
  }).isRequired,
  presetColor: PropTypes.string.isRequired,
  setPresetColor: PropTypes.func.isRequired,
}

const Customization = () => {
  const theme = useTheme()
  const dispatch = useDispatch()
  const customization = useSelector((state) => state.customization)

  const [open, setOpen] = useState(false)
  const handleToggle = () => setOpen(!open)

  const [borderRadius, setBorderRadius] = useState(customization.borderRadius)
  const [mode, setMode] = useState(customization.mode)

  const handleBorderRadius = (event, newValue) => {
    setBorderRadius(newValue)
  }

  useEffect(() => {
    dispatch(customizationActionCreator.setBorderRadius(borderRadius))
  }, [dispatch, borderRadius])

  const [colorPreset, setColorPreset] = useState(customization.colorPreset)

  useEffect(() => {
    dispatch(customizationActionCreator.setColorPreset(colorPreset))
  }, [dispatch, colorPreset])

  useEffect(() => {
    dispatch(customizationActionCreator.setMode(mode))
  }, [dispatch, mode])

  let initialFont
  switch (customization.fontFamily) {
    case `'Inter', sans-serif`:
      initialFont = 'Inter'
      break
    case `'Poppins', sans-serif`:
      initialFont = 'Poppins'
      break
    case `'Roboto', sans-serif`:
    default:
      initialFont = 'Roboto'
      break
  }

  const [fontFamily, setFontFamily] = useState(initialFont)

  useEffect(() => {
    let newFont
    switch (fontFamily) {
      case 'Inter':
        newFont = `'Inter', sans-serif`
        break
      case 'Poppins':
        newFont = `'Poppins', sans-serif`
        break
      case 'Roboto':
      default:
        newFont = `'Roboto', sans-serif`
        break
    }
    dispatch(customizationActionCreator.setFontFamily(newFont))
  }, [dispatch, fontFamily])

  const colorOptions = [
    {
      id: 'default',
      primary:
        theme.palette.mode === 'dark'
          ? colors.darkPrimaryMain
          : colors.primaryMain,
      secondary:
        theme.palette.mode === 'dark'
          ? colors.darkSecondaryMain
          : colors.secondaryMain,
    },
    {
      id: 'theme2',
      primary:
        theme.palette.mode === 'dark'
          ? theme2.darkPrimaryMain
          : theme2.primaryMain,
      secondary:
        theme.palette.mode === 'dark'
          ? theme2.darkSecondaryMain
          : theme2.secondaryMain,
    },
    {
      id: 'theme6',
      primary:
        theme.palette.mode === 'dark'
          ? theme6.darkPrimaryMain
          : theme6.primaryMain,
      secondary:
        theme.palette.mode === 'dark'
          ? theme6.darkSecondaryMain
          : theme6.secondaryMain,
    },
  ]

  return (
    <>
      <Tooltip
        title={
          <FormattedMessage
            id="live_customize"
            defaultMessage="Live Customize"
          />
        }
      >
        <Fab
          component="div"
          onClick={handleToggle}
          size="medium"
          variant="circular"
          color="secondary"
          sx={{
            borderRadius: 0,
            borderTopLeftRadius: '50%',
            borderBottomLeftRadius: '50%',
            borderTopRightRadius: '50%',
            borderBottomRightRadius: '4px',
            top: '25%',
            position: 'fixed',
            right: 10,
            zIndex: theme.zIndex.speedDial,
            boxShadow: theme.customShadows.secondary,
          }}
        >
          <AnimateButton type="rotate">
            <IconButton color="inherit" size="large" disableRipple>
              <IconSettings />
            </IconButton>
          </AnimateButton>
        </Fab>
      </Tooltip>
      <Drawer
        anchor="right"
        onClose={handleToggle}
        open={open}
        PaperProps={{
          sx: {
            width: 280,
          },
        }}
      >
        <PerfectScrollbar component="div">
          <Grid container spacing={3} sx={{ p: 3 }}>
            <Grid item xs={12}>
              <SubCard
                title={<FormattedMessage id="mode" defaultMessage="Mode" />}
              >
                <FormControl>
                  <RadioGroup
                    row
                    aria-label="layout"
                    value={mode}
                    onChange={(e) => setMode(e.target.value)}
                    name="row-radio-buttons-group"
                  >
                    <FormControlLabel
                      value="light"
                      control={<Radio />}
                      label="Light"
                      sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': {
                          color: theme.palette.grey[900],
                        },
                      }}
                    />
                    <FormControlLabel
                      value="dark"
                      control={<Radio />}
                      label="Dark"
                      sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': {
                          color: theme.palette.grey[900],
                        },
                      }}
                    />
                  </RadioGroup>
                </FormControl>
              </SubCard>
            </Grid>
            <Grid item xs={12}>
              <SubCard
                title={
                  <FormattedMessage
                    id="preset_color"
                    defaultMessage="Preset Color"
                  />
                }
              >
                <Grid item container spacing={2} alignItems="center">
                  {colorOptions.map((color) => (
                    <PresetColor
                      key={color.id}
                      color={color}
                      presetColor={colorPreset}
                      setPresetColor={setColorPreset}
                    />
                  ))}
                </Grid>
              </SubCard>
            </Grid>
            <Grid item xs={12}>
              {/* font family */}
              <SubCard
                title={
                  <FormattedMessage
                    id="font_family"
                    defaultMessage="Font Family"
                  />
                }
              >
                <FormControl>
                  <RadioGroup
                    aria-label="font-family"
                    value={fontFamily}
                    onChange={(e) => setFontFamily(e.target.value)}
                    name="row-radio-buttons-group"
                  >
                    <FormControlLabel
                      value="Roboto"
                      control={<Radio />}
                      label="Roboto"
                      sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': {
                          color: theme.palette.grey[900],
                        },
                      }}
                    />
                    <FormControlLabel
                      value="Poppins"
                      control={<Radio />}
                      label="Poppins"
                      sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': {
                          color: theme.palette.grey[900],
                        },
                      }}
                    />
                    <FormControlLabel
                      value="Inter"
                      control={<Radio />}
                      label="Inter"
                      sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': {
                          color: theme.palette.grey[900],
                        },
                      }}
                    />
                  </RadioGroup>
                </FormControl>
              </SubCard>
            </Grid>
            <Grid item xs={12}>
              {/* border radius */}
              <SubCard
                title={
                  <FormattedMessage
                    id="border_radius"
                    defaultMessage="Border Radius"
                  />
                }
              >
                <Grid
                  item
                  xs={12}
                  container
                  spacing={2}
                  alignItems="center"
                  sx={{ mt: 2.5 }}
                >
                  <Grid item>
                    <Typography variant="h6" color="primary">
                      4px
                    </Typography>
                  </Grid>
                  <Grid item xs>
                    <Slider
                      size="small"
                      value={borderRadius}
                      onChange={handleBorderRadius}
                      getAriaValueText={valueText}
                      valueLabelDisplay="on"
                      aria-labelledby="discrete-slider-small-steps"
                      marks
                      step={2}
                      min={4}
                      max={24}
                      color="primary"
                      sx={{
                        '& .MuiSlider-valueLabel': {
                          color:
                            theme.palette.mode === 'dark'
                              ? 'secondary.dark'
                              : 'secondary.light',
                        },
                      }}
                    />
                  </Grid>
                  <Grid item>
                    <Typography variant="h6" color="primary">
                      24px
                    </Typography>
                  </Grid>
                </Grid>
              </SubCard>
            </Grid>
          </Grid>
        </PerfectScrollbar>
      </Drawer>
    </>
  )
}

export default Customization

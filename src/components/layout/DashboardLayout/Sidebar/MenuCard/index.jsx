import React from 'react'
import { styled } from '@mui/material/styles'
import { Card, CardContent, Grid, Typography } from '@mui/material'

const CardStyle = styled(Card)(({ theme }) => ({
  background:
    theme.palette.mode === 'dark'
      ? theme.palette.dark[800]
      : theme.palette.primary.light,
  marginBottom: '22px',
  overflow: 'hidden',
  position: 'relative',
  '&:after': {
    content: '""',
    position: 'absolute',
    width: '157px',
    height: '157px',
    background: theme.palette.primary.main,
    borderRadius: '50%',
    top: '-115px',
    right: '-96px',
  },
}))

const MenuCard = () => (
  <CardStyle>
    <CardContent sx={{ p: 2, pt: 4 }}>
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Typography
            variant="subtitle2"
            color="grey.900"
            sx={{ opacity: 0.8 }}
          >
            “If there is magic on this planet, it is contained in water.” <br />
            — Loren Eiseley
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
  </CardStyle>
)

export default MenuCard

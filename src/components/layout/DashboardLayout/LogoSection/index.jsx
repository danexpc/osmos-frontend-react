import React from 'react'
import { Link } from 'react-router-dom'

import { ButtonBase, Card } from '@mui/material'
import { styled } from '@mui/material/styles'

import config from 'config'
import Logo from 'components/ui/Logo'

const LogoWrapper = styled(Card)(({ theme }) => ({
  marginLeft: '20px',
  backgroundColor: theme.palette.background.default,
}))

const LogoSection = () => (
  <ButtonBase disableRipple component={Link} to={config.defaultPath}>
    <LogoWrapper>
      <Logo />
    </LogoWrapper>
  </ButtonBase>
)

export default LogoSection

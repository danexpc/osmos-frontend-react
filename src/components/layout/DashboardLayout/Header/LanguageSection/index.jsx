import React, { useState, useRef, useEffect } from 'react'

import { useTheme } from '@mui/material/styles'
import {
  Avatar,
  Box,
  ButtonBase,
  ClickAwayListener,
  Grid,
  List,
  ListItemButton,
  ListItemText,
  Paper,
  Popper,
  Typography,
  useMediaQuery,
} from '@mui/material'
import TranslateTwoToneIcon from '@mui/icons-material/TranslateTwoTone'
import { useDispatch, useSelector } from 'react-redux'

import Transitions from 'components/ui/extended/Transitions'
import { customizationActionCreator } from 'store/actions'

const LanguageSection = () => {
  const theme = useTheme()
  const dispatch = useDispatch()
  const customization = useSelector((state) => state.customization)
  const matchesXs = useMediaQuery(theme.breakpoints.down('md'))
  const [language, setLanguage] = useState(customization.locale)

  const [open, setOpen] = useState(false)

  const handleListItemClick = (lng) => {
    setLanguage(lng)
    dispatch(customizationActionCreator.setLocale(lng))
    setOpen(false)
  }

  const anchorRef = useRef(null)

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }
    setOpen(false)
  }

  const prevOpen = useRef(open)
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus()
    }
    prevOpen.current = open
  }, [open])

  return (
    <>
      <Box
        sx={{
          ml: 2,
          mr: 3,
          [theme.breakpoints.down('md')]: {
            mr: 2,
          },
        }}
      >
        <ButtonBase sx={{ borderRadius: '12px' }}>
          <Avatar
            variant="rounded"
            sx={{
              ...theme.typography.commonAvatar,
              ...theme.typography.mediumAvatar,
              border: '1px solid',
              transition: 'all .2s ease-in-out',
              borderColor:
                theme.palette.mode === 'dark'
                  ? theme.palette.dark.main
                  : theme.palette.primary.light,
              background:
                theme.palette.mode === 'dark'
                  ? theme.palette.dark.main
                  : theme.palette.primary.light,
              '&[aria-controls="menu-list-grow"],&:hover': {
                background: theme.palette.primary.dark,
                color: theme.palette.primary.light,
              },
            }}
            ref={anchorRef}
            aria-controls={open ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            onClick={handleToggle}
            color="inherit"
          >
            {language !== 'en' && (
              <Typography
                variant="h5"
                sx={{ textTransform: 'uppercase' }}
                color="inherit"
              >
                {language}
              </Typography>
            )}
            {language === 'en' && (
              <TranslateTwoToneIcon sx={{ fontSize: '1.3rem' }} />
            )}
          </Avatar>
        </ButtonBase>
      </Box>
      <Popper
        placement={matchesXs ? 'bottom-start' : 'bottom'}
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        popperOptions={{
          modifiers: [
            {
              name: 'offset',
              options: {
                offset: [matchesXs ? 0 : 0, 20],
              },
            },
          ],
        }}
      >
        {({ TransitionProps }) => (
          <Transitions
            position={matchesXs ? 'top-left' : 'top'}
            in={open}
            {...TransitionProps}
          >
            <Paper elevation={16}>
              <ClickAwayListener onClickAway={handleClose}>
                <List
                  component="nav"
                  sx={{
                    width: '100%',
                    minWidth: 200,
                    maxWidth: 280,
                    bgcolor: theme.palette.background.paper,
                    borderRadius: customization.borderRadius,
                    [theme.breakpoints.down('md')]: {
                      maxWidth: 250,
                    },
                  }}
                >
                  <ListItemButton
                    selected={language === 'en'}
                    onClick={() => handleListItemClick('en')}
                  >
                    <ListItemText
                      primary={
                        <Grid container>
                          <Typography color="textPrimary">English</Typography>
                          <Typography
                            variant="caption"
                            color="textSecondary"
                            sx={{ ml: '8px' }}
                          >
                            (UK)
                          </Typography>
                        </Grid>
                      }
                    />
                  </ListItemButton>
                  <ListItemButton
                    selected={language === 'uk'}
                    onClick={() => handleListItemClick('uk')}
                  >
                    <ListItemText
                      primary={
                        <Grid container>
                          <Typography color="textPrimary">
                            Українська
                          </Typography>
                          <Typography
                            variant="caption"
                            color="textSecondary"
                            sx={{ ml: '8px' }}
                          >
                            (UA)
                          </Typography>
                        </Grid>
                      }
                    />
                  </ListItemButton>
                </List>
              </ClickAwayListener>
            </Paper>
          </Transitions>
        )}
      </Popper>
    </>
  )
}

export default LanguageSection

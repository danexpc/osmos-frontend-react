import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'

import { useTheme, styled } from '@mui/material/styles'
import {
  Autocomplete,
  Avatar,
  Box,
  ButtonBase,
  Card,
  Grid,
  InputAdornment,
  Popper,
  TextField,
} from '@mui/material'

import { IconSearch, IconX } from '@tabler/icons'
import { shouldForwardProp } from '@mui/system'
import PopupState, { bindPopper, bindToggle } from 'material-ui-popup-state'

import Transitions from 'components/ui/extended/Transitions'
import * as headerActionCreator from 'store/header/actions'
import { PaginationEnum } from 'common/enums/enums'
import { isLoading } from 'utils/utils'

const PopperStyle = styled(Popper, { shouldForwardProp })(({ theme }) => ({
  zIndex: 1100,
  width: '99%',
  top: '-55px !important',
  padding: '0 12px',
  backgroundColor: theme.palette.background.default,
  [theme.breakpoints.down('sm')]: {
    padding: '0 10px',
  },
}))

const AutocompleteStyle = styled(Autocomplete, { shouldForwardProp })(
  ({ theme }) => ({
    width: 434,
    marginLeft: 16,
    paddingLeft: 16,
    paddingRight: 16,
    '& input': {
      background: 'transparent !important',
      paddingLeft: '4px !important',
    },
    [theme.breakpoints.down('lg')]: {
      width: 250,
    },
    [theme.breakpoints.down('md')]: {
      width: '100%',
      marginLeft: 4,
      backgroundColor:
        theme.palette.mode === 'dark'
          ? theme.palette.background.default
          : '#fff',
    },
  })
)

const HeaderAvatarStyle = styled(Avatar, { shouldForwardProp })(
  ({ theme }) => ({
    ...theme.typography.commonAvatar,
    ...theme.typography.mediumAvatar,
    background:
      theme.palette.mode === 'dark'
        ? theme.palette.dark.main
        : theme.palette.secondary.light,
    color:
      theme.palette.mode === 'dark'
        ? theme.palette.secondary.main
        : theme.palette.secondary.dark,
    '&:hover': {
      background:
        theme.palette.mode === 'dark'
          ? theme.palette.secondary.main
          : theme.palette.secondary.dark,
      color:
        theme.palette.mode === 'dark'
          ? theme.palette.secondary.light
          : theme.palette.secondary.light,
    },
  })
)

const MobileSelect = ({
  open,
  loading,
  setOpen,
  popupState,
  options,
  selected,
  handleOpen,
  handleSelectBranch,
  handleInputChange,
}) => {
  const theme = useTheme()

  return (
    <AutocompleteStyle
      id="select-header"
      open={open}
      placeholder="Select branch"
      onOpen={handleOpen}
      value={selected}
      loading={isLoading(loading)}
      onClose={() => setOpen(false)}
      isOptionEqualToValue={(option, value) => option.name === value.name}
      getOptionLabel={(option) => option.name}
      onChange={(_, value) => handleSelectBranch(value)}
      onInputChange={(_, value) => handleInputChange(value)}
      options={isLoading(loading) ? [] : options || []}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Branch"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                <InputAdornment position="end">
                  <Box sx={{ ml: 2 }}>
                    <ButtonBase sx={{ borderRadius: '12px' }}>
                      <Avatar
                        variant="rounded"
                        sx={{
                          ...theme.typography.commonAvatar,
                          ...theme.typography.mediumAvatar,
                          background:
                            theme.palette.mode === 'dark'
                              ? theme.palette.dark.main
                              : theme.palette.orange.light,
                          color: theme.palette.orange.dark,
                          '&:hover': {
                            background: theme.palette.orange.dark,
                            color: theme.palette.orange.light,
                          },
                        }}
                        {...bindToggle(popupState)}
                      >
                        <IconX stroke={1.5} size="1.3rem" />
                      </Avatar>
                    </ButtonBase>
                  </Box>
                </InputAdornment>
              </>
            ),
          }}
        />
      )}
    />
  )
}

/* eslint-disable react/forbid-prop-types */
MobileSelect.propTypes = {
  open: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  popupState: PropTypes.object.isRequired,
  options: PropTypes.array.isRequired,
  selected: PropTypes.object.isRequired,
  handleOpen: PropTypes.func.isRequired,
  handleSelectBranch: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
}

const SelectSection = () => {
  const theme = useTheme()
  const dispatch = useDispatch()

  const [open, setOpen] = useState(false)

  const {
    branches: { data },
    selectedBranch,
    loading,
  } = useSelector((state) => state.header)

  const handleOpen = () => {
    dispatch(
      headerActionCreator.loadBranches({
        limit: PaginationEnum.DEFAULT_LIMIT,
        offset: PaginationEnum.DEFAULT_OFFSET,
      })
    )
    setOpen(true)
  }

  const handleSelectBranch = (value) => {
    dispatch(headerActionCreator.setSelectedBranch(value))
  }

  const handleInputChange = (value) => {
    dispatch(
      headerActionCreator.loadBranches({
        limit: PaginationEnum.DEFAULT_LIMIT,
        offset: PaginationEnum.DEFAULT_OFFSET,
        keyword: value,
      })
    )
  }

  return (
    <>
      <Box sx={{ display: { xs: 'block', md: 'none' } }}>
        <PopupState variant="popper" popupId="demo-popup-popper">
          {(popupState) => (
            <>
              <Box sx={{ ml: 2 }}>
                <ButtonBase sx={{ borderRadius: '12px' }}>
                  <HeaderAvatarStyle
                    variant="rounded"
                    {...bindToggle(popupState)}
                  >
                    <IconSearch stroke={1.5} size="1.2rem" />
                  </HeaderAvatarStyle>
                </ButtonBase>
              </Box>
              <PopperStyle {...bindPopper(popupState)} transition>
                {({ TransitionProps }) => (
                  <>
                    <Transitions
                      type="zoom"
                      {...TransitionProps}
                      sx={{ transformOrigin: 'center left' }}
                    >
                      <Card
                        sx={{
                          backgroundColor: theme.palette.background.default,
                          [theme.breakpoints.down('sm')]: {
                            border: 0,
                            boxShadow: 'none',
                          },
                        }}
                      >
                        <Box sx={{ p: 2 }}>
                          <Grid
                            container
                            alignItems="center"
                            justifyContent="space-between"
                          >
                            <Grid item xs>
                              <MobileSelect
                                open={open}
                                loading={loading}
                                setOpen={setOpen}
                                popupState={popupState}
                                options={data || []}
                                selected={selectedBranch || {}}
                                handleOpen={handleOpen}
                                handleSelectBranch={handleSelectBranch}
                                handleInputChange={handleInputChange}
                              />
                            </Grid>
                          </Grid>
                        </Box>
                      </Card>
                    </Transitions>
                  </>
                )}
              </PopperStyle>
            </>
          )}
        </PopupState>
      </Box>
      <Box sx={{ display: { xs: 'none', md: 'block' } }}>
        <AutocompleteStyle
          id="select-header"
          open={open}
          placeholder="Select branch"
          onOpen={handleOpen}
          value={selectedBranch || {}}
          loading={isLoading(loading)}
          onChange={(_, value) => handleSelectBranch(value)}
          onInputChange={(_, value) => handleInputChange(value)}
          onClose={() => setOpen(false)}
          isOptionEqualToValue={(option, value) => option.id === value.id}
          getOptionLabel={(option) => option.name}
          options={isLoading(loading) ? [] : data || []}
          disableClearable
          renderInput={(params) => <TextField {...params} label="Branch" />}
        />
      </Box>
    </>
  )
}

export default SelectSection

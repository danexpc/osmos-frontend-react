const config = {
  defaultPath: '/',
  fontFamily: `'Roboto', sans-serif`,
  borderRadius: 12,
  defaultLocale: 'en',
}

export default config

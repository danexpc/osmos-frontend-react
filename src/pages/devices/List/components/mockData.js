const headCells = [
  {
    id: 'id',
    numeric: true,
    label: 'ID',
    align: 'center',
  },
  {
    id: 'name',
    numeric: false,
    label: 'Product Name',
    align: 'left',
  },
  {
    id: 'category',
    numeric: false,
    label: 'Category',
    align: 'left',
  },
  {
    id: 'price',
    numeric: true,
    label: 'Price',
    align: 'right',
  },
  {
    id: 'date',
    numeric: true,
    label: 'Date',
    align: 'center',
  },
  {
    id: 'qty',
    numeric: true,
    label: 'QTY',
    align: 'right',
  },
]

const createData = (id, name, category, price, date, qty) => ({
  id,
  name,
  category,
  price,
  date,
  qty,
})

const rowsInitial = [
  createData(
    '790841',
    'Samsung TV 32” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '790842',
    'Iphone 11 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '798699',
    'Samsung TV 34” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '790752',
    'Iphone 12 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '790955',
    'Samsung TV 36” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '790785',
    'Iphone 13 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '800837',
    'Samsung TV 38” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '810365',
    'Iphone 14 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '810814',
    'Samsung TV 40” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '820385',
    'Iphone 15 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '820885',
    'Samsung TV 42” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '830390',
    'Iphone 16 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '830879',
    'Samsung TV 44” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '900111',
    'Iphone 17 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '900836',
    'Samsung TV 46” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '900112',
    'Iphone 18 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '900871',
    'Samsung TV 48” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '910232',
    'Iphone 19 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
  createData(
    '910886',
    'Samsung TV 50” LED Retina',
    'Television',
    2500,
    '12.07.2018',
    5
  ),
  createData(
    '910232',
    'Iphone 20 Pro Max',
    'Television',
    5000,
    '12.07.2018',
    2
  ),
]

export { headCells, rowsInitial }

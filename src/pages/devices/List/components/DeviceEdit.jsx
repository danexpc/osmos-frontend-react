import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'

import { Grid, MenuItem, TextField } from '@mui/material'

import ModalForm from '../../../../components/ui/forms/ModalForm'

const schema = yup.object().shape({
  name: yup.string().min(3).max(72).required(),
  ownerId: yup.number().required(),
})

const categories = [
  {
    value: 1,
    label: 'User #1',
  },
  {
    value: 2,
    label: 'User #2',
  },
  {
    value: 3,
    label: 'User #3',
  },
  {
    value: 4,
    label: 'User #4',
  },
]

const DeviceEdit = ({ open, handleCloseDialog }) => {
  const [ownerId, setOwnerId] = useState(2)

  const {
    register,
    handleSubmit,
    formState: { errors, touchedFields },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  })

  const onSubmitHandler = (data) => {
    console.log({ data })
    reset()
  }

  const handleSelectChange = (event) => {
    if (event?.target.value) setOwnerId(event?.target.value)
  }

  return (
    <ModalForm
      handleCloseDialog={handleCloseDialog}
      open={open}
      handleSubmit={handleSubmit(onSubmitHandler)}
      title="Edit Device"
    >
      <Grid item xs={12}>
        <TextField
          {...register('name')}
          id="outlined-basic1"
          fullWidth
          label="Enter Branch Name*"
          error={touchedFields.name && Boolean(errors.name)}
          helperText={touchedFields.name && errors.name && errors.name.message}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          {...register('description')}
          id="outlined-basic2"
          fullWidth
          multiline
          rows={3}
          label="Enter Branch Description"
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          {...register('ownerId')}
          id="standard-select-currency"
          select
          label="Select Owner*"
          value={ownerId}
          fullWidth
          onChange={handleSelectChange}
          error={touchedFields.ownerId && Boolean(errors.ownerId)}
          helperText={
            touchedFields.ownerId && errors.ownerId && errors.ownerId.message
          }
        >
          {categories.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </Grid>
    </ModalForm>
  )
}

DeviceEdit.propTypes = {
  open: PropTypes.bool.isRequired,
  handleCloseDialog: PropTypes.func.isRequired,
}

export default DeviceEdit

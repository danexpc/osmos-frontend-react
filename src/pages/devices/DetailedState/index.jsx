import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Box, Tab, Tabs } from '@mui/material'

import MainCard from 'components/ui/cards/MainCard'
import SystemInfo from './components/SystemInfo'
import SystemIndicators from './components/SystemIndicators'
import NotificationSettings from './components/NotificationSettings'
import MaintenanceLog from './components/MaintenanceLog'
import NotificationLogs from './components/NotificationLogs'

/* eslint-disable react/forbid-prop-types */
const TabPanel = ({ children, value, index, ...other }) => (
  <div
    role="tabpanel"
    hidden={value !== index}
    id={`simple-tabpanel-${index}`}
    aria-labelledby={`simple-tab-${index}`}
    {...other}
  >
    {value === index && <Box sx={{ p: 0 }}>{children}</Box>}
  </div>
)

TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

const a11yProps = (index) => ({
  id: `simple-tab-${index}`,
  'aria-controls': `simple-tabpanel-${index}`,
})

const DetailedState = () => {
  const [value, setValue] = useState(0)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <MainCard>
      <div>
        <Tabs
          value={value}
          indicatorColor="primary"
          onChange={handleChange}
          sx={{
            mb: 3,
            minHeight: 'auto',
            '& button': {
              minWidth: 100,
            },
            '& a': {
              minHeight: 'auto',
              minWidth: 10,
              py: 1.5,
              px: 1,
              mr: 2.25,
              color: 'grey.600',
            },
            '& a.Mui-selected': {
              color: 'primary.main',
            },
          }}
          aria-label="simple tabs example"
          variant="scrollable"
        >
          <Tab component={Link} to="#" label="System info" {...a11yProps(0)} />
          <Tab
            component={Link}
            to="#"
            label="System indicators"
            {...a11yProps(1)}
          />
          <Tab
            component={Link}
            to="#"
            label="Notification settings"
            {...a11yProps(2)}
          />
          <Tab
            component={Link}
            to="#"
            label="Notification Logs"
            {...a11yProps(3)}
          />
          <Tab
            component={Link}
            to="#"
            label="Maintenance Log"
            {...a11yProps(4)}
          />
        </Tabs>
        <TabPanel value={value} index={0}>
          <SystemInfo />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <SystemIndicators />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <NotificationSettings />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <NotificationLogs />
        </TabPanel>
        <TabPanel value={value} index={4}>
          <MaintenanceLog />
        </TabPanel>
      </div>
    </MainCard>
  )
}

export default DetailedState

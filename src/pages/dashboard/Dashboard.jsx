import React, { useEffect, useState } from 'react'
import { Grid } from '@mui/material'
import { gridSpacing } from 'themes/common/constants'
import TodayPurifiedCard from './components/TodayPurifiedCard'
import LastMonthPurifiedCard from './components/LastMonthPurifiedCard'
import LastMonthGrowthBarChart from './components/LastMonthGrowthBarChart'

const Dashboard = () => {
  const [isLoading, setLoading] = useState(true)
  useEffect(() => {
    setLoading(false)
  }, [])

  return (
    <Grid container spacing={gridSpacing}>
      <Grid item xs={12}>
        <Grid container spacing={gridSpacing}>
          <Grid item lg={4} md={6} sm={6} xs={12}>
            <TodayPurifiedCard isLoading={isLoading} />
          </Grid>
          <Grid item lg={4} md={6} sm={6} xs={12}>
            <LastMonthPurifiedCard isLoading={isLoading} />
          </Grid>
          <Grid item lg={4} md={6} sm={6} xs={12}>
            <LastMonthPurifiedCard isLoading={isLoading} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <LastMonthGrowthBarChart isLoading={isLoading} />
      </Grid>
    </Grid>
  )
}

export default Dashboard

import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

import { Grid, Typography } from '@mui/material'
import { useTheme } from '@mui/styles'

import ApexCharts from 'apexcharts'
import Chart from 'react-apexcharts'

import { FormattedMessage } from 'react-intl'

import MainCard from 'components/ui/cards/MainCard'
import { gridSpacing } from 'themes/common/constants'
import chartData from '../chartData'

const LastMonthGrowthBarChart = ({ isLoading }) => {
  const theme = useTheme()
  const { mode } = useSelector((state) => state.customization)

  const { primary } = theme.palette.text
  const darkLight = theme.palette.dark.light
  const grey200 = theme.palette.grey[200]
  const grey500 = theme.palette.grey[500]

  const primary200 = theme.palette.primary[200]
  const primaryDark = theme.palette.primary.dark
  const secondaryMain = theme.palette.secondary.main
  const secondaryLight = theme.palette.secondary.light

  useEffect(() => {
    const newChartData = {
      ...chartData,
      colors: [primaryDark],
      xaxis: {
        labels: {
          style: {
            colors: [
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
              primary,
            ],
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            colors: [primary],
          },
        },
      },
      grid: {
        borderColor: mode === 'dark' ? darkLight + 20 : grey200,
      },
      tooltip: {
        theme: mode,
      },
      legend: {
        labels: {
          colors: grey500,
        },
      },
    }

    if (!isLoading) {
      ApexCharts.exec('bar-chart', 'updateOptions', newChartData)
    }
  }, [
    mode,
    primary200,
    primaryDark,
    secondaryMain,
    secondaryLight,
    primary,
    darkLight,
    grey200,
    isLoading,
    grey500,
  ])

  return (
    <>
      {isLoading ? (
        <></>
      ) : (
        <MainCard>
          <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
              <Grid container direction="column" spacing={1}>
                <Grid item>
                  <Typography variant="subtitle2">
                    <FormattedMessage
                      id="total_purified"
                      defaultMessage="Total Purified"
                    />
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="h3">
                    2 543 000 <FormattedMessage id="l" defaultMessage="L" />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Chart {...chartData} />
            </Grid>
          </Grid>
        </MainCard>
      )}
    </>
  )
}

LastMonthGrowthBarChart.propTypes = {
  isLoading: PropTypes.bool.isRequired,
}

export default LastMonthGrowthBarChart

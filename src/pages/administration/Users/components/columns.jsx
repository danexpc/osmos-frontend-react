import React from 'react'
import { FormattedMessage } from 'react-intl'

const headCells = [
  {
    id: 'id',
    numeric: true,
    label: <FormattedMessage id="id" defaultMessage="ID" />,
    align: 'center',
    sortable: true,
  },
  {
    id: 'name',
    numeric: false,
    label: <FormattedMessage id="name" defaultMessage="Name" />,
    align: 'left',
    sortable: true,
  },
  {
    id: 'role',
    numeric: false,
    label: <FormattedMessage id="role" defaultMessage="Role" />,
    align: 'left',
    sortable: true,
  },
  {
    id: 'branch',
    numeric: false,
    label: <FormattedMessage id="branch" defaultMessage="Branch" />,
    align: 'left',
    sortable: false,
  },
]

export { headCells }

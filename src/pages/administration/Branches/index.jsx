import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Button,
  Grid,
  InputAdornment,
  OutlinedInput,
  Pagination,
  Tooltip,
} from '@mui/material'
import { FormattedMessage, injectIntl } from 'react-intl'
import PropTypes from 'prop-types'
import { IconSearch } from '@tabler/icons'
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined'

import MainCard from 'components/ui/cards/MainCard'
import { gridSpacing } from 'themes/common/constants'
import { calculateOffset, countPagesAmount } from 'helpers/pagination.helper'
import * as branchesActionCreator from 'store/administration/branches/actions'

import PageSizer from 'components/helpers/PageSizer'
import { PageSize, PaginationEnum } from 'common/enums/enums'

import BranchList from './components/BranchList'
import BranchAdd from './components/BranchAdd'
import BranchEdit from './components/BranchEdit'
import useDialog from '../../../hooks/useDialog'
import usePagination from '../../../hooks/usePagination'
import useSearch from '../../../hooks/useSearch'
import useSort from '../../../hooks/useSort'

const Branches = ({ intl }) => {
  const dispatch = useDispatch()

  const [rows, setRows] = useState([])

  const {
    openAdd,
    openEdit,
    handleOpenAddDialog,
    handleCloseAddDialog,
    handleOpenEditDialog,
    handleCloseEditDialog,
  } = useDialog()

  const {
    anchorEl,
    page,
    pageSize,
    setPage,
    handlePageSizerClick,
    handlePageSizerClose,
    handlePageChange,
  } = usePagination()

  const { keyword, handleSearch } = useSearch()

  const { orderBy, orderType, handleRequestSort } = useSort()

  const { branches } = useSelector((state) => state.branches)

  useEffect(() => {
    dispatch(
      branchesActionCreator.getBranches({
        limit: PaginationEnum.DEFAULT_LIMIT,
        offset: PaginationEnum.DEFAULT_OFFSET,
      })
    )
  }, [])

  useEffect(() => {
    if (branches) {
      setRows(branches.data)
    }
  }, [branches])

  const handlePageNChange = (p) =>
    handlePageChange(
      p,
      branchesActionCreator.getBranches({
        limit: pageSize,
        offset: calculateOffset(p, pageSize),
        keyword,
        orderBy,
        orderType,
      })
    )

  const handleSearchByKeyword = (e) => {
    handleSearch(
      e,
      branchesActionCreator.getBranches({
        limit: pageSize,
        offset: PaginationEnum.DEFAULT_OFFSET,
        keyword: e.target.value,
        orderBy,
        orderType,
      })
    )
    setPage(PaginationEnum.DEFAULT_PAGE)
  }

  const handleSort = (property) => {
    handleRequestSort(property, (newOrderBy, newOrderType) =>
      branchesActionCreator.getBranches({
        limit: pageSize,
        offset: PaginationEnum.DEFAULT_OFFSET,
        keyword,
        orderBy: newOrderBy,
        orderType: newOrderType,
      })
    )
    setPage(PaginationEnum.DEFAULT_PAGE)
  }

  const handleSelectPageClose = (ps) =>
    handlePageSizerClose(
      ps,
      branchesActionCreator.getBranches({
        limit: ps,
        offset: PaginationEnum.DEFAULT_OFFSET,
        keyword,
        orderBy,
        orderType,
      })
    )

  return (
    <MainCard
      title={
        <Grid
          container
          alignItems="center"
          justifyContent="space-between"
          spacing={gridSpacing}
        >
          <Grid item>
            <Tooltip title={<FormattedMessage id="add" defaultMessage="Add" />}>
              <Button
                variant="contained"
                size="medium"
                startIcon={<AddCircleOutlineOutlinedIcon />}
                sx={{ px: 2.75, py: 1.5 }}
                onClick={handleOpenAddDialog}
              >
                <FormattedMessage id="add" defaultMessage="Add" />
              </Button>
            </Tooltip>
            <BranchAdd
              open={openAdd}
              handleCloseDialog={handleCloseAddDialog}
            />
          </Grid>
          <Grid item>
            <OutlinedInput
              id="input-search-list"
              placeholder={intl.formatMessage({ id: 'search' })}
              onChange={handleSearchByKeyword}
              startAdornment={
                <InputAdornment position="start">
                  <IconSearch stroke={1.5} size="1rem" />
                </InputAdornment>
              }
              size="small"
            />
          </Grid>
        </Grid>
      }
      content={false}
    >
      <BranchList
        rows={rows}
        orderBy={orderBy}
        orderType={orderType}
        handleRequestSort={handleSort}
        handleOpenDialog={handleOpenEditDialog}
      />
      <BranchEdit open={openEdit} handleCloseDialog={handleCloseEditDialog} />
      <Grid item xs={12} sx={{ p: 3 }}>
        <Grid container justifyContent="space-between" spacing={gridSpacing}>
          <Grid item>
            <Pagination
              count={branches ? countPagesAmount(branches.total, pageSize) : 1}
              onChange={(_, p) => handlePageNChange(p)}
              page={page}
              color="primary"
            />
          </Grid>
          <Grid item>
            <PageSizer
              anchorEl={anchorEl}
              pageSize={pageSize}
              handleClick={handlePageSizerClick}
              handleClose={handleSelectPageClose}
              sizeOptions={[
                PageSize.SIZE_10,
                PageSize.SIZE_20,
                PageSize.SIZE_50,
              ]}
            />
          </Grid>
        </Grid>
      </Grid>
    </MainCard>
  )
}

/* eslint-disable react/forbid-prop-types */
Branches.propTypes = {
  intl: PropTypes.object.isRequired,
}

export default injectIntl(Branches)

import React from 'react'
import PropTypes from 'prop-types'

import { useTheme } from '@mui/material/styles'
import {
  Box,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  Typography,
} from '@mui/material'
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone'
import { visuallyHidden } from '@mui/utils'
import { FormattedMessage } from 'react-intl'

import { OrderType } from 'common/enums/enums'

import { headCells } from './columns'

const EnhancedTableHead = ({ order, orderBy, onRequestSort, theme }) => {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.align}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell?.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell sortDirection={false} align="center" sx={{ pr: 3 }}>
          <Typography
            variant="subtitle1"
            sx={{
              color: theme.palette.mode === 'dark' ? 'grey.600' : 'grey.900',
            }}
          >
            <FormattedMessage id="action" defaultMessage="Action" />
          </Typography>
        </TableCell>
      </TableRow>
    </TableHead>
  )
}

/* eslint-disable react/forbid-prop-types */
EnhancedTableHead.propTypes = {
  theme: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf([OrderType.ASC, OrderType.DESC, OrderType.NONE]),
  orderBy: PropTypes.string,
}

EnhancedTableHead.defaultProps = {
  order: OrderType.NONE,
  orderBy: null,
}

const BranchList = ({
  rows,
  orderType,
  orderBy,
  handleRequestSort,
  handleOpenDialog,
}) => {
  const theme = useTheme()

  return (
    <TableContainer>
      <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
        <EnhancedTableHead
          order={orderType}
          orderBy={orderBy}
          onRequestSort={(_, property) => {
            handleRequestSort(property)
          }}
          theme={theme}
        />
        <TableBody>
          {rows.map((row) => {
            const labelId = `enhanced-table-checkbox-${row.id}`

            return (
              <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                <TableCell
                  align="center"
                  component="th"
                  id={labelId}
                  scope="row"
                  onClick={() => {}}
                  sx={{ cursor: 'pointer' }}
                >
                  <Typography
                    variant="subtitle1"
                    sx={{
                      color:
                        theme.palette.mode === 'dark' ? 'grey.600' : 'grey.900',
                    }}
                  >
                    {row.id}
                  </Typography>
                </TableCell>
                <TableCell
                  component="th"
                  id={labelId}
                  scope="row"
                  onClick={() => {}}
                  sx={{ cursor: 'pointer' }}
                >
                  <Typography
                    variant="subtitle1"
                    sx={{
                      color:
                        theme.palette.mode === 'dark' ? 'grey.600' : 'grey.900',
                    }}
                  >
                    {' '}
                    {row.name}{' '}
                  </Typography>
                </TableCell>
                <TableCell align="center" sx={{ pr: 3 }}>
                  <IconButton
                    color="primary"
                    size="large"
                    onClick={handleOpenDialog}
                  >
                    <EditTwoToneIcon sx={{ fontSize: '1.3rem' }} />
                  </IconButton>
                </TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

BranchList.propTypes = {
  rows: PropTypes.array.isRequired,
  orderType: PropTypes.string,
  orderBy: PropTypes.string,
  handleRequestSort: PropTypes.func.isRequired,
  handleOpenDialog: PropTypes.func.isRequired,
}

BranchList.defaultProps = {
  orderType: OrderType.NONE,
  orderBy: null,
}

export default BranchList

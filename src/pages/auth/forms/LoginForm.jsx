import React, { useState } from 'react'
import { useDispatch } from 'react-redux'

import { useTheme } from '@mui/material/styles'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useForm } from 'react-hook-form'
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  Typography,
} from '@mui/material'

import { FormattedMessage } from 'react-intl'

import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'
import AnimateButton from 'components/ui/extended/AnimateButton'
import { authActionCreator } from 'store/actions'

const schema = yup
  .object({
    login: yup.string().required(),
    password: yup.string().required(),
  })
  .required()

const LoginForm = ({ ...others }) => {
  const theme = useTheme()
  const dispatch = useDispatch()
  const [checked, setChecked] = useState(true)
  const [showPassword, setShowPassword] = useState(false)

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword)
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  })

  const onSubmit = (data) => {
    dispatch(authActionCreator.login(data))
  }

  return (
    <>
      <form noValidate onSubmit={handleSubmit(onSubmit)} {...others}>
        <FormControl
          fullWidth
          error={false}
          sx={{ ...theme.typography.customInput }}
        >
          <InputLabel htmlFor="outlined-adornment-email-login">
            <FormattedMessage id="email" defaultMessage="Email Address" /> /{' '}
            <FormattedMessage id="username" defaultMessage="Username" />
          </InputLabel>
          <OutlinedInput
            {...register('login')}
            id="outlined-adornment-email-login"
            type="email"
            name="login"
            label={`${(
              <FormattedMessage id="email" defaultMessage="Email Address" />
            )} / ${(
              <FormattedMessage id="username" defaultMessage="Username" />
            )}`}
            inputProps={{}}
          />
          {errors.login && (
            <FormHelperText error id="standard-weight-helper-text-email-login">
              {errors.login.message}
            </FormHelperText>
          )}
        </FormControl>

        <FormControl
          fullWidth
          error={false}
          sx={{ ...theme.typography.customInput }}
        >
          <InputLabel htmlFor="outlined-adornment-password-login">
            <FormattedMessage id="password" defaultMessage="Password" />
          </InputLabel>
          <OutlinedInput
            {...register('password')}
            id="outlined-adornment-password-login"
            type={showPassword ? 'text' : 'password'}
            name="password"
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                  size="large"
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            label={<FormattedMessage id="password" defaultMessage="Password" />}
            inputProps={{}}
          />
          {errors.password && (
            <FormHelperText
              error
              id="standard-weight-helper-text-password-login"
            >
              {errors.password.message}
            </FormHelperText>
          )}
        </FormControl>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          spacing={1}
        >
          <FormControlLabel
            control={
              <Checkbox
                checked={checked}
                onChange={() => setChecked(!checked)}
                name="checked"
                color="primary"
              />
            }
            label={
              <FormattedMessage id="remember_me" defaultMessage="Remember me" />
            }
          />
          <Typography
            variant="subtitle1"
            color="primary"
            sx={{ textDecoration: 'none', cursor: 'pointer' }}
          >
            <FormattedMessage
              id="forgot_password"
              defaultMessage="Forgot password"
            />
            ?
          </Typography>
        </Stack>

        <Box sx={{ mt: 2 }}>
          <AnimateButton>
            <Button
              disabled={false}
              disableElevation
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              style={{ backgroundColor: theme.palette.primary.dark }}
            >
              <FormattedMessage id="sign_in" defaultMessage="Sign In" />
            </Button>
          </AnimateButton>
        </Box>
      </form>
    </>
  )
}

export default LoginForm

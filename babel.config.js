module.exports = {
  presets: ['@babel/preset-react'],
  plugins: [
    '@babel/plugin-proposal-export-namespace-from',
    ['module-resolver', { root: 'src' }],
  ],
}

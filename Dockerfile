FROM node:17.5.0 as builder
RUN mkdir /src
WORKDIR /src
COPY . .
RUN NODE_OPTIONS="--max-old-space-size=8192"
ENV GENERATE_SOURCEMAP false
RUN npm install && \
    yarn install && \
    yarn build && \
    pwd && \
    ls -la && \
    ls -la /src/build

FROM nginx:stable
COPY --from=builder /src/build /usr/share/nginx/html
ADD  nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
